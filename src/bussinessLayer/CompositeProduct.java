package bussinessLayer;

import java.util.ArrayList;

public class CompositeProduct implements MenuItem {
	
	private String nume;
	private ArrayList<MenuItem> specialOffer = new ArrayList<MenuItem>();
	
	
	
	public ArrayList<MenuItem> getSpecialOffer() {
		return this.specialOffer;
	}

	public void setSpecialOffer(ArrayList<MenuItem> specialOffer) {
		this.specialOffer = specialOffer;
	}

	public CompositeProduct(String nume, ArrayList<MenuItem> specialOfer) {
		super();
		this.nume = nume;
		this.specialOffer = specialOffer;
	}

	public float computePrice() {
		int sum=0;
		for(MenuItem mi: specialOffer) {
			sum += mi.computePrice();
		}
		return sum;
	}

	public String getName() {
		return nume;
	}
}
