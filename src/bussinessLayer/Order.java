package bussinessLayer;

public class Order{
	
	private int orderID;
	private static int counterId=0;
	private String date;
	private int tableNr;
	
	public Order() {
		super();
		this.orderID=++counterId;
	}
	
	public int getOrderID() {
		return orderID;
	}
	
	public Order(String date, int tableNr) {
		super();
		this.date = date;
		this.tableNr = tableNr;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public int getTableNr() {
		return tableNr;
	}
	public void setTableNr(int tableNr) {
		this.tableNr = tableNr;
	}

	@Override
	public String toString() {
		return "Order [orderID=" + orderID + ", date=" + date + ", tableNr=" + tableNr + "]";
	}
	
	
}
