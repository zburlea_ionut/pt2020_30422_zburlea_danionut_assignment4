package bussinessLayer;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Observable;

public class Restaurant extends Observable{
	
	private ArrayList<MenuItem> menu;
	private Map<Order, ArrayList<MenuItem>> hashMap;
	private MenuItem specialOffer;
	
	public Restaurant() {
		this.setHashMap(new HashMap<Order, ArrayList<MenuItem>>());
		this.setMenu(new ArrayList<MenuItem>());
	}
	
	public ArrayList<MenuItem> getMenu() {
		return menu;
	}
	
	public void setMenu(ArrayList<MenuItem> menu) {
		this.menu = menu;
	}
	
	public Map<Order, ArrayList<MenuItem>> getHashMap() {
		return hashMap;
	}
	
	public void setHashMap(Map<Order, ArrayList<MenuItem>> hashMap) {
		this.hashMap = hashMap;
	}
	
	public MenuItem createMenuItem(String name, float price) {
		assert(price > 0);
		BaseProduct baseProduct = new BaseProduct(name, price);
		return baseProduct;
	}

	public void editMenuItem(String name, float price) {
		assert(price > 0);
		BaseProduct baseProduct = new BaseProduct(name, price);
		
		for(MenuItem menuItem : this.menu) {
			if(menuItem.getName().equals(name)) {
				int index = this.menu.indexOf(menuItem);
				this.menu.set(index, baseProduct);
			}
		}
	
	}
	public MenuItem createSpecialOffer(String nume, ArrayList<MenuItem> specialOfferItems) {
		for(MenuItem me: specialOfferItems) {
			assert(me.computePrice() > 0);
		}
		
		CompositeProduct compProd= new CompositeProduct(nume, specialOfferItems);
		this.specialOffer = compProd;
		return compProd;
		
	}
	public void addMenuItem(MenuItem menuItem) {
		menu.add(menuItem);
	}
	
	public void deleteMenuItem(String name) {
		for(int i = 0;  i <menu.size(); i++) {
			if(name.equals(menu.get(i).getName())) {
				menu.remove(i);
			}
		}
	}
	
	public Order createOrder( String date, int tableNr, ArrayList<MenuItem> orderItems) {
		Order order = new Order();
		order.setDate(date);
		order.setTableNr(tableNr);
		this.hashMap.put(order, orderItems);
		setChanged();
		hasChanged();
		notifyObservers();
		return order;
	}
	public float orderTotalPrice(Order order) {
		ArrayList<MenuItem> orderItems = this.hashMap.get(order);
		float totalPrice = 0;
		for(MenuItem menuItem : orderItems) {
			totalPrice = totalPrice + menuItem.computePrice();
		}
		return totalPrice;
	}
	
	public void writeBill(Order order) throws FileNotFoundException {
		PrintWriter printWriter = new PrintWriter("Bill.txt");
		ArrayList<MenuItem> orderItems = this.hashMap.get(order);
		float totalPrice = orderTotalPrice(order);
		for(MenuItem menuItem : orderItems) {
			printWriter.print(menuItem.getName() + " ");
			printWriter.print(menuItem.computePrice() + "\n");
		}
		printWriter.println("Total = "+totalPrice);
		printWriter.close();
	}
	
	@Override
	public String toString() {
		return "Restaurant [menu=" + menu + ", hashMap=" + hashMap + "]";
	}

	public MenuItem getSpecialOffer() {
		return specialOffer;
	}

	public void setSpecialOffer(MenuItem specialOffer) {
		this.specialOffer = specialOffer;
	}
	
	
}
