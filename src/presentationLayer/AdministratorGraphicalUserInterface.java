package presentationLayer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import bussinessLayer.Restaurant;

import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class AdministratorGraphicalUserInterface extends JFrame {

	private JPanel contentPane;

	public AdministratorGraphicalUserInterface(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 483, 487);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel lblWelcomeAdministrator = new JLabel("Welcome, administrator!");
		lblWelcomeAdministrator.setFont(new Font("Tahoma", Font.BOLD, 20));
		lblWelcomeAdministrator.setBounds(36, 46, 367, 42);
		contentPane.add(lblWelcomeAdministrator);
		
		JLabel lblPleaseSelectWhat = new JLabel("Please select what action you want to perform:");
		lblPleaseSelectWhat.setFont(new Font("Tahoma", Font.PLAIN, 18));
		lblPleaseSelectWhat.setBounds(36, 101, 390, 22);
		contentPane.add(lblPleaseSelectWhat);
		
		JButton btnCreateNewMenu = new JButton("Create new menu item");
		btnCreateNewMenu.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				CreateMenuItem createMenuItem = new CreateMenuItem(restaurant);
				createMenuItem.setVisible(true);
				setVisible(false);
			}
		});
		btnCreateNewMenu.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnCreateNewMenu.setBounds(35, 153, 236, 33);
		contentPane.add(btnCreateNewMenu);
		
		JButton btnNewButton = new JButton("Delete menu item");
		btnNewButton.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				DeleteMenuItem deleteMenuItem = new DeleteMenuItem(restaurant);
				deleteMenuItem.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton.setBounds(36, 199, 236, 33);
		contentPane.add(btnNewButton);
		
		JButton btnEditMenuItem = new JButton("Edit menu item");
		btnEditMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				EditMenuItem editMenuItem = new EditMenuItem(restaurant);
				editMenuItem.setVisible(true);
				setVisible(false);
			}
		});
		btnEditMenuItem.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnEditMenuItem.setBounds(36, 245, 236, 33);
		contentPane.add(btnEditMenuItem);
		
		JButton btnNewButton_1 = new JButton("View all menu");
		btnNewButton_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewMenu viewMenu = new ViewMenu(restaurant);
				viewMenu.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton_1.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton_1.setBounds(36, 291, 236, 33);
		contentPane.add(btnNewButton_1);
		
		JButton btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				MainMenu mainMenu = new MainMenu(restaurant);
				mainMenu.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(12, 23, 97, 25);
		contentPane.add(btnGoBack);
		
		JButton btnNewButton_2 = new JButton("Create special offer");
		btnNewButton_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				SpecialOffer specialOffer = new SpecialOffer(restaurant);
				specialOffer.setVisible(true);
				setVisible(false);
			}
		});
		btnNewButton_2.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnNewButton_2.setBounds(36, 337, 236, 33);
		contentPane.add(btnNewButton_2);
		
		JButton btnViewSpecialOffer = new JButton("View special offer");
		btnViewSpecialOffer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ViewSpecialOffer viewSpecialOffer = new ViewSpecialOffer(restaurant);
				viewSpecialOffer.setVisible(true);
				setVisible(false);
			}
		});
		btnViewSpecialOffer.setFont(new Font("Tahoma", Font.PLAIN, 20));
		btnViewSpecialOffer.setBounds(36, 383, 236, 33);
		contentPane.add(btnViewSpecialOffer);
	}
}
