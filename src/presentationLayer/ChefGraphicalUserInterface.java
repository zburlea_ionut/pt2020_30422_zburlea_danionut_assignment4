package presentationLayer;

import java.awt.BorderLayout;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

import bussinessLayer.MenuItem;
import bussinessLayer.Order;
import bussinessLayer.Restaurant;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ChefGraphicalUserInterface extends JFrame implements Observer{
	private JPanel contentPane;
	private JTable table;
	JPanel panel = new JPanel();
	private Object[] columsName;
	private Object[] rowData;
	private Restaurant restaurant;
	
	public ChefGraphicalUserInterface(Restaurant restaurant) {
		this.restaurant = restaurant;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 690, 445);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		JButton btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ChefGraphicalUserInterface  chefGraphicalUserInterface = new ChefGraphicalUserInterface(restaurant);
				chefGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(12, 13, 97, 25);
		getContentPane().add(btnGoBack);
		
		JPanel panel = new JPanel();
		panel.setBounds(123, 75, 451, 266);
		panel.setLayout(new BorderLayout(0, 0));
		
		 Map<Order, ArrayList<MenuItem>> map = restaurant.getHashMap();
		
		String[] colName= new String[] {"Id", "Data", "Table", "Products"};
		Object[][] data = new Object[map.size()][4];
		
		int row=-1;
		Iterator it = map.entrySet().iterator();
		while(it.hasNext()) {
			row += 1;
			Map.Entry pair = (Map.Entry)it.next();
			data[row][0] =((Order) pair.getKey()).getOrderID();
			data[row][1]= ((Order) pair.getKey()).getDate();
			data[row][2]= ((Order) pair.getKey()).getTableNr();
			
			String orderItemsString = " ";
            for (MenuItem menuItem: (ArrayList<MenuItem>) pair.getValue()) {
                orderItemsString +=" "+menuItem.getName();
               
            }
            data[row][3] = orderItemsString;
		}
		
		table = new JTable(data,colName);
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		
		
		
		contentPane.add(panel);
		
	}

	@Override
	public void update(Observable arg0, Object arg1) {
		removeAll();
		JPanel panel = new JPanel();
		panel.setBounds(123, 75, 451, 266);
		panel.setLayout(new BorderLayout(0, 0));
		
		 Map<Order, ArrayList<MenuItem>> map = restaurant.getHashMap();
		
		String[] colName= new String[] {"Id", "Data", "Table", "Products"};
		Object[][] data = new Object[map.size()][4];
		
		int row=-1;
		Iterator it = map.entrySet().iterator();
		while(it.hasNext()) {
			row += 1;
			Map.Entry pair = (Map.Entry)it.next();
			data[row][0] =((Order) pair.getKey()).getOrderID();
			data[row][1]= ((Order) pair.getKey()).getDate();
			data[row][2]= ((Order) pair.getKey()).getTableNr();
			
			String orderItemsString = " ";
            for (MenuItem menuItem: (ArrayList<MenuItem>) pair.getValue()) {
                orderItemsString +=" "+menuItem.getName();
               
            }
            data[row][3] = orderItemsString;
		}
		
		table = new JTable(data,colName);
		panel.add(new JScrollPane(table), BorderLayout.CENTER);
		
		
		
		contentPane.add(panel);
		
	}

}
