package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bussinessLayer.MenuItem;
import bussinessLayer.Order;
import bussinessLayer.Restaurant;

import java.awt.EventQueue;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.event.ActionEvent;

public class ComputePrice extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	private JPanel contentPane;
	private JButton btnGoBack;
	
	public ComputePrice(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 418, 261);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		JLabel lblInsertOrderid = new JLabel("Insert orderId:");
		lblInsertOrderid.setBounds(33, 54, 94, 29);
		getContentPane().add(lblInsertOrderid);
		
		textField = new JTextField();
		textField.setBounds(126, 57, 116, 22);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(126, 134, 144, 40);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnComputePrice = new JButton("Compute price");
		btnComputePrice.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				float totalPrice = 0;
				String orderIDString = textField.getText();
				int orderID = Integer.parseInt(orderIDString);
				for(HashMap.Entry<Order, ArrayList<MenuItem>> entry : restaurant.getHashMap().entrySet()) {
					if(entry.getKey().getOrderID() == orderID) {
						totalPrice = restaurant.orderTotalPrice(entry.getKey());
					}
				}
				String totalPriceString = String.valueOf(totalPrice);
				textField_1.setText(totalPriceString);
			}
		});
		btnComputePrice.setBounds(126, 92, 129, 29);
		getContentPane().add(btnComputePrice);
		
		btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WaiterGraphicalUserInterface waiterGraphicalUserInterface = new WaiterGraphicalUserInterface(restaurant);
				waiterGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(0, 115, 97, 25);
		contentPane.add(btnGoBack);
	}

}
