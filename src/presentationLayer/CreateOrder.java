package presentationLayer;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bussinessLayer.MenuItem;
import bussinessLayer.Restaurant;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.awt.event.ActionEvent;

public class CreateOrder extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JPanel contentPane;

	public CreateOrder(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 418, 261);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		JLabel lblInsertTableNumber = new JLabel("Insert table number:");
		lblInsertTableNumber.setBounds(12, 58, 135, 16);
		getContentPane().add(lblInsertTableNumber);
		
		textField_1 = new JTextField();
		textField_1.setBounds(132, 55, 116, 22);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblInsertDate = new JLabel("Insert date:");
		lblInsertDate.setBounds(12, 93, 87, 16);
		getContentPane().add(lblInsertDate);
		
		textField_2 = new JTextField();
		textField_2.setBounds(132, 90, 116, 22);
		getContentPane().add(textField_2);
		textField_2.setColumns(10);
		
		textField_3 = new JTextField();
		textField_3.setBounds(132, 125, 116, 22);
		getContentPane().add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblInsertOrderItems = new JLabel("Insert order items:");
		lblInsertOrderItems.setBounds(12, 128, 116, 16);
		getContentPane().add(lblInsertOrderItems);
		
		JButton btnNewButton = new JButton("New button");
		btnNewButton.setBounds(64, 226, 116, -22);
		contentPane.add(btnNewButton);
		
		JButton btnCreateOrder = new JButton("Create order");
		btnCreateOrder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				ArrayList<MenuItem> orderItems = new ArrayList<MenuItem>();
				String tableNrString = textField_1.getText();
				String date = textField_2.getText();
				String orderItemsString = textField_3.getText();
				String[] orderItemsStrings = orderItemsString.split(",");
				for(String orderItem : orderItemsStrings) {
					for(MenuItem menuItem : restaurant.getMenu()) {
						if(orderItem.equals(menuItem.getName())) {
							orderItems.add(menuItem);
						}
					}
				}
				int tableNr = Integer.parseInt(tableNrString);
				restaurant.createOrder(date, tableNr, orderItems);
			}
		});
		btnCreateOrder.setBounds(132, 160, 116, 36);
		contentPane.add(btnCreateOrder);
		
		JButton btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WaiterGraphicalUserInterface waiterGraphicalUserInterface = new WaiterGraphicalUserInterface(restaurant);
				waiterGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(12, 166, 97, 25);
		contentPane.add(btnGoBack);
	}
}
