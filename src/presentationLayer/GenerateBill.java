package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bussinessLayer.MenuItem;
import bussinessLayer.Order;
import bussinessLayer.Restaurant;

import java.awt.EventQueue;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.awt.event.ActionEvent;

public class GenerateBill extends JFrame{
	private JTextField textField;
	private JPanel contentPane;
	private JButton btnGoBack;
	
	public GenerateBill(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 379, 219);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		JLabel lblInsertOrderid = new JLabel("Insert orderID:");
		lblInsertOrderid.setBounds(26, 32, 96, 27);
		getContentPane().add(lblInsertOrderid);
		
		textField = new JTextField();
		textField.setBounds(134, 34, 116, 22);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnGenerateBill = new JButton("Generate bill");
		btnGenerateBill.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String orderIDString = textField.getText();
				int orderID = Integer.parseInt(orderIDString);
				for(HashMap.Entry<Order, ArrayList<MenuItem>> entry : restaurant.getHashMap().entrySet()) {
					if(entry.getKey().getOrderID() == orderID) {
						try {
							restaurant.writeBill(entry.getKey());
						} catch (FileNotFoundException e1) {
							e1.printStackTrace();
						}
					}
				}
			}
		});
		btnGenerateBill.setBounds(134, 69, 123, 38);
		getContentPane().add(btnGenerateBill);
		
		btnGoBack = new JButton("go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				WaiterGraphicalUserInterface waiterGraphicalUserInterface = new WaiterGraphicalUserInterface(restaurant);
				waiterGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(12, 76, 97, 25);
		contentPane.add(btnGoBack);
	}

}
