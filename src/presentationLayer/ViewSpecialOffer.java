package presentationLayer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import bussinessLayer.Restaurant;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class ViewSpecialOffer extends JFrame{
	private JTextField textField;
	private JTextField textField_1;
	private JPanel contentPane;
	public ViewSpecialOffer(Restaurant restaurant) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 464, 285);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		getContentPane().setLayout(null);
		
		JLabel lblSpecialOfferItems = new JLabel("Special offer items:");
		lblSpecialOfferItems.setBounds(12, 83, 117, 21);
		getContentPane().add(lblSpecialOfferItems);
		
		textField = new JTextField();
		textField.setBounds(141, 82, 116, 22);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JLabel lblSpecialOfferPrice = new JLabel("Special offer price: ");
		lblSpecialOfferPrice.setBounds(12, 129, 117, 21);
		getContentPane().add(lblSpecialOfferPrice);
		
		textField_1 = new JTextField();
		textField_1.setBounds(141, 128, 116, 22);
		getContentPane().add(textField_1);
		textField_1.setColumns(10);
		
		JButton btnViewSpecialOffer = new JButton("View special offer");
		btnViewSpecialOffer.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String specialOfferName = restaurant.getSpecialOffer().getName();
				String specialOfferTotalPrice = String.valueOf(restaurant.getSpecialOffer().computePrice());
				textField.setText(specialOfferName);
				textField_1.setText(specialOfferTotalPrice);
			}
		});
		btnViewSpecialOffer.setBounds(160, 180, 160, 25);
		getContentPane().add(btnViewSpecialOffer);
		
		JButton btnGoBack = new JButton("Go back");
		btnGoBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministratorGraphicalUserInterface administratorGraphicalUserInterface = new AdministratorGraphicalUserInterface(restaurant);
				administratorGraphicalUserInterface.setVisible(true);
				setVisible(false);
			}
		});
		btnGoBack.setBounds(12, 180, 97, 25);
		getContentPane().add(btnGoBack);
	}

}
